// We include base program and set few defined variables

// Number of times manycore repeats the test
#define BUF_REPEAT 2

// We always start from core 0 in the X-axis
#define X_CORD_START 0

// Include the base program main c-code file
#include "manycore_streambuf.c"
