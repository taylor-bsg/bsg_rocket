//========================================================================
// ubmark-memcpy-data.h
//========================================================================
// memcpy data

// Definitions
#include "memcpy_defs.h"

#ifndef __MEMCPY_DATA_H
#define __MEMCPY_DATA_H

// Filler data to ensure input-data is isolated in its own page
word_t filler_data_before[2 * PAGE_SIZE / sizeof(word_t)] __attribute__((aligned(0x1000)));

// Actual data
word_t data_random[] __attribute__((aligned(0x1000))) = 
{
  #include "memcpy_random_data.h"
};

// Data size
#define DATA_SIZE (sizeof(data_random) / sizeof(word_t))

// Filler to guard
word_t filler_data_output[2 * PAGE_SIZE / sizeof(word_t)] __attribute__((aligned(0x1000)));

#endif //__MEMCPY_DATA_H
