//====================================================================
// bsg_rocket_remote_load.c
// 02/21/2018, shawnless.xie@gmail.com
//====================================================================

#include <assert.h>
#include <stdio.h>
#include <stdint.h>

#include "bsg_rocket_rocc.h"


#define BUF_LEN 16
#define DM_ADDR 0x1000
/////////////////////////////////////////////////////////////
int main() {

  int read_value[BUF_LEN];

  for( int i=0; i<16; i++){
    bsg_rocc_write( 0x0 , 0x0,  (DM_ADDR + i*4), i ) ;
  }

  asm ("fence;");

 for( int i=0; i<16; i++){
    bsg_rocc_read( 0x0, 0x0,  (DM_ADDR + i*4), read_value[i]);
 }

 for( int i=0; i<16; i++){
    printf(" Read value: %0x, Expected:%0x, ", read_value[i], i);
    if( read_value[i] != i ) printf(" ==> Error\n");
    else                  printf(" ==> Pass\n");
 }

 return 0;
}
