//====================================================================
// bsg_rocket_manycore_chainded_core.c
// 02/08/2016, shawnless.xie@gmail.com
//====================================================================
// The following is a RISC-V program to test synchronization of tiles.
// All tiles are chained together and pass data through

#include <assert.h>
#include <stdio.h>
#include <stdint.h>

#include "bsg_manycore_buffer.h"
#include "bsg_rocket_rocc.h"
#include "manycore.cfg.h"
#include "chained_core.h"

////////////////////////////////////////////////////////////
//
int   waiting_cycle_limit = 100000;

void init_source( int y_cord, int x_cord, \
            volatile manycore_task_s * pMCViewTask,  \
            volatile manycore_task_s * pRocketViewTask);

int check_result(volatile manycore_task_s *pRocketViewTask);

/////////////////////////////////////////////////////////////
int main() {
  volatile manycore_task_s * pRocketViewTask =bsg_rocket_view_task( & manycore_data_s, \
                                                                      manycore_mem_vect);

  //setup the segment address
  bsg_rocc_seg_addr(  manycore_mem_vect );

  //load memory into each tiles
  for( int i=0; i< bsg_tiles_Y; i ++ ) {
    for( int j=0; j< bsg_tiles_X; j ++ ) {
        bsg_rocc_load_manycore(i, j);
    }
  }

  //We only have to initialize the last tile
  init_source(bsg_tiles_Y-1, bsg_tiles_X-1, &manycore_data_s, pRocketViewTask);

  //invoke each tile
  printf(" ==> Invoking manycore... \n");
  for( int i=0; i< bsg_tiles_Y; i ++ ) {
    for( int j=0; j< bsg_tiles_X; j ++ ) {
        bsg_rocc_unfreeze(i, j);
    }
  }

  int i = bsg_rocc_poll_task( pRocketViewTask, waiting_cycle_limit );

  if( i == waiting_cycle_limit )
    printf(" ==> FAIL ! wait %d but not result return \n", waiting_cycle_limit );
  else if( check_result( pRocketViewTask ) )
    printf(" ==> FAIL ! received data is not correct \n");
  else
    printf(" ==> PASS !\n");

  //shut down  each tile
  for( int i=0; i< bsg_tiles_Y; i ++ ) {
    for( int j=0; j< bsg_tiles_X; j ++ ) {
        bsg_rocc_freeze(i, j);
    }
  }

 return 0;
}

/////////////////////////////////////////////////////////////
//

tag_data_s  expect[ BUF_LEN ];

int get_expect_result() {
   int  tiles_num = bsg_tiles_X * bsg_tiles_Y ;
   int  tiles_id_check_sum =0;
   for(int i=0; i< tiles_num -1 ; i ++)  tiles_id_check_sum += i;

   for(int i=0; i< BUF_LEN;  i++){
    expect[i].tag.cores     = tiles_id_check_sum;
    expect[i].tag.rounds    = MAX_ROUND_NUM-1   ;
    expect[i].tag.value     = i;
   }
}

//init the source data
void init_source( int y_cord, int x_cord, \
            volatile manycore_task_s * pMCViewTask,  \
            volatile manycore_task_s * pRocketViewTask){
  //setup the base address
  bsg_rocc_write( y_cord, x_cord, &(pMCViewTask->base_addr), manycore_mem_vect);

  //reset the done signal.
  pRocketViewTask->done = 0;
}

//check the result
int check_result(volatile manycore_task_s *pRocketViewTask){

    get_expect_result();

    int error = 0;
    tag_data_s  result;

    for( int i=0; i< BUF_LEN ; i ++ ){
            result.data = pRocketViewTask->result[i];
            printf(" ==> [%d]:       Rounds=%08x,\tValue=%08x,\tcores=%08x,\n", i,
                           result.tag.rounds, result.tag.value, result.tag.cores);

            printf("         Expect: Rounds=%08x,\tValue=%08x,\tcores=%08x,",
                          expect[i].tag.rounds, expect[i].tag.value, expect[i].tag.cores);

        if( result.data != expect[i].data ) { printf(" Incorrect.\n" ); error = 1; }
        else                                { printf(" Correct.\n");               }
    }
    return error;
}
