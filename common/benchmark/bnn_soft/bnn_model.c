#include "bnn_common.h"

//------------------------------------------------------------------------
// Proxy function to run a layer on riscv
//------------------------------------------------------------------------
void run_layer_riscv(
           Word     *data_i       ,
           Word     *data_o       ,
           Word     *wt_array     ,
           Word     *kh_array     ,
           uint32_t  n_inputs     ,
           uint32_t  n_outputs    ,
           uint32_t  width        ,
           uint32_t  layer_idx    ,
           uint32_t  input_words  ,
           uint32_t  output_words ,
           uint32_t  dmem_mode    ,
  const    uint32_t  layer_type   , // 0=conv1, 1=conv, 2=dense, 3=last
  const    uint32_t  max_pool     ,
           bool      isSneakPath
);

//-------------------------------------------------------------------
// Compute and execute an xcel schedule
//-------------------------------------------------------------------
void run_layer_riscv(
           Word     *data_i      ,
           Word     *data_o      ,
           Word     *wt_array    ,
           Word     *kh_array    ,
           uint32_t  n_inputs    ,
           uint32_t  n_outputs   ,
           uint32_t  width       ,
           uint32_t  layer_idx   ,
           uint32_t  input_words ,
           uint32_t  output_words,
           uint32_t  dmem_mode   ,
  const    uint32_t  layer_type  , // 0=conv1, 1=conv, 2=dense, 3=last
  const    uint32_t  max_pool    ,
           bool      isSneakPath
){
  assert (wt_array != NULL);
  assert (kh_array != NULL);
  const uint32_t width_mode = width >> 4;

  // for conv layers
  uint32_t width_o = 0;
  // imgs_per_batch is the number of output images to compute per batch
  uint32_t imgs_per_batch = 0;

  uint32_t n_batches;

  if (layer_type < LAYER_DENSE) {
    width_o = (max_pool==0) ? width : width / 2;
    imgs_per_batch = find_conv_batch_size(width, width_o, n_inputs, n_outputs);
  }
  else {
    width_o = 1;
    imgs_per_batch = find_dense_batch_size(n_inputs, n_outputs);
  }
  n_batches = n_outputs / imgs_per_batch;

  assert (imgs_per_batch != 0);
  assert (n_outputs % imgs_per_batch == 0); // TODO: this should not be necessary

  uint32_t o = 0;
  for (uint32_t batch = 0; batch < n_batches; batch++) {
    // layer_mode[0] is 1 iff first invocation of a layer
    // layer_mode[2:1] is the layer_type
    uint32_t layer_mode = ((layer_type & 0x3) << 1) | ((o == 0) ? 1 : 0);
    uint32_t wt_idx;
    uint32_t wt_offset;

    Word *wt = NULL;
    Word *kh = NULL;

    uint32_t quant     = 0;
    uint32_t wt_bits   = 0;

    if (layer_type == LAYER_CONV1) {
      wt_bits   = n_inputs * imgs_per_batch * WT_SIZE;
      quant     = CONV_W_PER_WORD;
    }
    else if (layer_type == LAYER_CONV) {
      wt_bits   = n_inputs * imgs_per_batch * WT_SIZE;
      quant     = CONV_W_PER_WORD;
    }
    else {
      wt_bits   = n_inputs * imgs_per_batch;
      quant     = WORD_SIZE;
    }

    wt_idx    =  (o * n_inputs) / quant           ;
    wt_offset = ((o * n_inputs) % quant) * WT_SIZE;

    // Weights
    wt = &wt_array[wt_idx];

    // KH
    kh = &kh_array[o / KH_PER_WORD];

    uint32_t i_words = (batch == 0            ) ? input_words  : 0;
    uint32_t o_words = (batch == n_batches - 1) ? output_words : 0;

    bnn(
        wt           , kh              ,
        data_i       , data_o          ,
        n_inputs     , imgs_per_batch  ,
        i_words      , o_words         ,
        layer_mode   , dmem_mode       ,
        width_mode   , max_pool + 1
    );

    o += imgs_per_batch;
  }
}

//------------------------------------------------------------------------
// Print helpers
//------------------------------------------------------------------------
void print_params(Bit params[CONVOLVERS][K][K]) {
  for (uint32_t m = 0; m < CONVOLVERS; ++m) {
    for (uint32_t wr = 0; wr < K; ++wr) {
      for (uint32_t wc = 0; wc < K; ++wc) {
        printf ("%3d", (params[m][wr][wc]==0) ? 0 : 1);
      }
      printf("\n");
    }
    printf("--\n");
  }
}

void print_line_buffer_m(TwoBit lbuf[CONV_BANKS][CONV_ROWS][CONV_COLS]) {
  for (uint32_t wr = 0; wr < CONV_ROWS; ++wr) {
  for (uint32_t bank = 0; bank < CONV_BANKS; ++bank) {
    for (uint32_t wc = 0; wc < CONV_COLS; ++wc) {
      printf ("%3d", lbuf[bank][wr][wc]);
    }
    printf (" |");
  }
  printf ("\n");
  }
}

//------------------------------------------------------------------------
// Load kh values
//------------------------------------------------------------------------
void load_kh_model(KType *ki, HType *hi, const Word kh_mem[KH_WORDS], Address idx) {
  // the KH_OFFSET must match the one in bnn-random.c for that test to work!
  const unsigned KH_OFFSET = 2;
  get_reference_kh(ki, hi, KH_OFFSET);
}

// -----------------------------------------------------------------------
// Conv
// -----------------------------------------------------------------------
ConvOut conv3x3b(
    const TwoBit line_buffer_m[CONV_BANKS][CONV_ROWS][CONV_COLS],
    const Bit conv_params_m[K][K],
    const uint32_t bank,
    const IdxType cc
) {
  ConvOut sum = 0;
  for (uint32_t kr = 0; kr < K; ++kr) {
    for (uint32_t kc = 0; kc < K; ++kc) {
      TwoBit data = line_buffer_m[bank][kr][cc+kc];
      const Bit wt = conv_params_m[2-kr][2-kc];
      data = (wt != 0) ? (TwoBit)(-data) : data;
      sum += data;
    }
  }
  return sum;
}

// -----------------------------------------------------------------------
// Produce 32 elements of conv results
// -----------------------------------------------------------------------
void conv_halfword(
    const TwoBit line_buffer_m[CONV_BANKS][CONV_ROWS][CONV_COLS],
    const Bit conv_params_m[K][K],
    ConvOut conv_out_buffer_m[WORD_SIZE/2]
) {
  for (uint32_t bank = 0; bank < CONV_BANKS; ++bank) {
    for (uint32_t cc = 0; cc < CONV_COLS-2; ++cc) {
      conv_out_buffer_m[bank*8+cc] = conv3x3b( line_buffer_m, conv_params_m, bank, cc );
    }
  }
}

// -----------------------------------------------------------------------
// Process each line in a word, we need to outline this loop to
// avoid false control dependencies in Vivado HLS
// -----------------------------------------------------------------------
void process_word(
    Word word,
    TwoBit  line_buffer_m[CONV_BANKS][CONV_ROWS][CONV_COLS],
    const   Bit conv_params_m[K][K],
    ConvOut conv_out_buffer_m[2][WORD_SIZE/2],
    const   uint32_t w_mode,
    const   uint32_t words_per_image,
    const   IdxType wrd
) {
  // Prologue: fill the last image line left from the previous iter
  for (uint32_t bank = 0; bank < CONV_BANKS; ++bank) {
    signed char s_idx = bank - CONV_BANKS + (1<<w_mode);
    const bool rd0 = !(bank==0 || w_mode==0 || (bank==2&&w_mode==1));
    const bool rd9 = !(bank==CONV_BANKS-1 || w_mode==0 || (bank==1&&w_mode==1));

    if (s_idx >= 0) {
      for (uint32_t cc = 0; cc < CONV_COLS-2; ++cc) {
        uint32_t w = (word >> (s_idx*8+cc) & 0x1);
#if defined(_RISCV) && (!defined(_ROCKET) || (defined(_ROCKET) && !defined(_PK)))
        const TwoBit data = (w == 0) ? (TwoBit)1 : (TwoBit)-1;
#else
        const TwoBit data = (w == 0) ? TwoBit(1) : TwoBit(-1);
#endif
        line_buffer_m[bank][CONV_ROWS-1][cc+1] = data;
      }

      TwoBit d0 = 0;
      uint32_t w0 = (word >> (s_idx*8-1)) & 0x1;
#if defined(_RISCV) && (!defined(_ROCKET) || (defined(_ROCKET) && !defined(_PK)))
      if (rd0) d0 = (w0 == 0) ? (TwoBit)1 : (TwoBit)-1;
#else
      if (rd0) d0 = (w0 == 0) ? TwoBit(1) : TwoBit(-1);
#endif
      line_buffer_m[bank][CONV_ROWS-1][0] = d0;
      TwoBit d9 = 0;
      uint32_t w9 = (word >> (s_idx*8+8)) & 0x1;
#if defined(_RISCV) && (!defined(_ROCKET) || (defined(_ROCKET) && !defined(_PK)))
      if (rd9) d9 = (w9 == 0) ? (TwoBit)1 : (TwoBit)-1;
#else
      if (rd9) d9 = (w9 == 0) ? TwoBit(1) : TwoBit(-1);
#endif
      line_buffer_m[bank][CONV_ROWS-1][9] = d9;
    }
  }

  // Each 64-bit word requires 2 iterations to process
  for (uint32_t hw = 0; hw < 2; ++hw) {
    DB(4,
      printf("Accel lbuf wrd%d, hw%d:\n", wrd, hw);
      print_line_buffer_m(line_buffer_m);
    );

    conv_halfword( line_buffer_m, conv_params_m, conv_out_buffer_m[hw] );

    // loop over each row and column in the collection
    // of line buffers and fill them with data
    for (uint32_t cr = 0; cr < CONV_ROWS; ++cr) {
      for (uint32_t bank = 0; bank < CONV_BANKS; ++bank) {
        // slice index
        signed char s_idx = ((cr-1)<<w_mode) + bank + hw*CONV_BANKS;
        const bool rd0 = !(bank==0 || w_mode==0 || (bank==2&&w_mode==1));
        const bool rd9 = !(bank==CONV_BANKS-1 || w_mode==0 || (bank==1&&w_mode==1));

        // accessing a line in the past or future!
        if (s_idx < 0 || s_idx > 7) {
          for (uint32_t cc = 0; cc < CONV_COLS; ++cc) {
#if defined(_RISCV) && (!defined(_ROCKET) || (defined(_ROCKET) && !defined(_PK)))
            const TwoBit x = (wrd == 0 || s_idx > 7) ? (TwoBit)0 : line_buffer_m[bank][3-w_mode][cc];
#else
            const TwoBit x = (wrd == 0 || s_idx > 7) ? TwoBit(0) : line_buffer_m[bank][3-w_mode][cc];
#endif
            line_buffer_m[bank][cr][cc] = x;
          }
        }
        // acessing a line in the present
        else {
          // insert data
          for (uint32_t cc = 0; cc < CONV_COLS-2; ++cc) {
            uint32_t w = (word >> (s_idx*8+cc) & 0x1);
#if defined(_RISCV) && (!defined(_ROCKET) || (defined(_ROCKET) && !defined(_PK)))
            const TwoBit data = (w == 0) ? (TwoBit)1 : (TwoBit)-1;
#else
            const TwoBit data = (w == 0) ? TwoBit(1) : TwoBit(-1);
#endif
            line_buffer_m[bank][cr][cc+1] = data;
          }

          TwoBit d0 = 0;
          uint32_t w0 = (word >> (s_idx*8-1)) & 0x1;
#if defined(_RISCV) && (!defined(_ROCKET) || (defined(_ROCKET) && !defined(_PK)))
          if (rd0) d0 = (w0 == 0) ? (TwoBit)1 : (TwoBit)-1;
#else
          if (rd0) d0 = (w0 == 0) ? TwoBit(1) : TwoBit(-1);
#endif
          line_buffer_m[bank][cr][0] = d0;
          TwoBit d9 = 0;
          uint32_t w9 = (word >> (s_idx*8+8)) & 0x1;
#if defined(_RISCV) && (!defined(_ROCKET) || (defined(_ROCKET) && !defined(_PK)))
          if (rd9) d9 = (w9 == 0) ? (TwoBit)1 : (TwoBit)-1;
#else
          if (rd9) d9 = (w9 == 0) ? TwoBit(1) : TwoBit(-1);
#endif
          line_buffer_m[bank][cr][9] = d9;
        }
      }
    }

  } // half-word

  DB(4,
    printf("Accel lbuf end\n");
    print_line_buffer_m(line_buffer_m);
  );
}

// -----------------------------------------------------------------------
// A single PE reads from all inputs and weights to generate a single
// output feature map.
// * Make sure this function gets inlined by VHLS, or cosim may fail!
// -----------------------------------------------------------------------
void bin_conv(
    Word wt_mem[CONVOLVERS][C_WT_WORDS],
    KType ki,
    HType hi,
    Word dmem[2][CONVOLVERS][C_DMEM_WORDS],
    uint8_t dmem_i_idx,
    uint8_t dmem_o_idx,
    const uint32_t   n_inputs,
    const Address    o_index,
    const uint32_t   new_batch,
    const uint32_t   width_mode,  // 0=8'b, 1=16'b, 2=32'b
    const uint32_t   norm_mode    // 0='do nothing', 1='do norm', 2='do pool'
) {
  const uint32_t log_width = width_mode + 3;
  const uint32_t words_per_image = 1 << (2*width_mode);
  const uint32_t n_phases = n_inputs / CONVOLVERS;
  const uint32_t images_per_phase = PIX_PER_PHASE >> (2*log_width);
  //const uint32_t WORDS_PER_PHASE = PIX_PER_PHASE / WORD_SIZE;

  assert(n_phases % images_per_phase == 0);
  assert(n_inputs % images_per_phase == 0);
  assert(images_per_phase*words_per_image == WORDS_PER_PHASE);

  // ---------------------------------------------------------------------
  // buffers
  // ---------------------------------------------------------------------
  TwoBit  line_buffer[CONVOLVERS][CONV_BANKS][CONV_ROWS][CONV_COLS];
  Bit     conv_params[CONVOLVERS][K][K];
  ConvSum fixed_buffer[WORDS_PER_PHASE][2][WORD_SIZE/2];
  ConvSum fixed_temp[2][WORD_SIZE/2];
  // per-convolver buffers
  Word    word_buffer[CONVOLVERS];
  Word    wt_word_buffer[CONVOLVERS];
  ConvOut conv_out_buffer[CONVOLVERS][2][WORD_SIZE/2];

  static Address wt_addr = 0;           // address of weight word
  static uint32_t wt_offset = 0;      // offset 0..6 of param
  if (new_batch != 0) { wt_addr = 0; wt_offset = 0; }

  // ---------------------------------------------------------------------

  // Reset conv buffer
  for (uint32_t i = 0; i < WORDS_PER_PHASE; ++i) {
    for (uint32_t j = 0; j < WORD_SIZE/2; ++j) {
      fixed_buffer[i][0][j] = 0;
      fixed_buffer[i][1][j] = 0;
    }
  }

  // RZ: this is not neccesary but avoids a warning
  for (uint32_t i = 0; i < CONVOLVERS; ++i)
    wt_word_buffer[i] = 0;

  // ---------------------------------------------------------------------
  // Compute in phases
  // Each phase processes CONVOLVERS * WORDS_PER_PHASE input words
  // ---------------------------------------------------------------------
  for (uint32_t p = 0; p < n_phases; p += images_per_phase) {
    DB(3, printf ("=== PHASE %d ===\n", p) );

    // wrd = which word in the current image
    // wrd_phase = which wrd in the current phase
    uint32_t wrd = 0;
    uint32_t wrd_phase = 0;

    // Load a word each iteration, and then process it
    // We load WORDS_PER_PHASE words per phase, however we also need 1 extra "empty"
    // iteration per image in the phase to do the loop epilogue, so the loop bound
    // is WORDS_PER_PHASE + images_per_phase
    for (uint32_t count = 0; count < WORDS_PER_PHASE+images_per_phase; ++count) {
      // First word of an image
      if (wrd == 0) {
        // -------------------------------------------------------------------
        // Load param word
        // Each word contains CONV_W_PER_WORD weight filters, after we use
        // them all we should load the next word
        // -------------------------------------------------------------------
        for (IdxType m = 0; m < CONVOLVERS; ++m) {
          if (wt_offset == 0)
            wt_word_buffer[m] = wt_mem[m][wt_addr];
          else
            wt_word_buffer[m] = wt_word_buffer[m] >> WT_SIZE;
        }
        if (wt_offset == CONV_W_PER_WORD-1) {
          ++wt_addr;
          wt_offset = 0;
        } else {
          ++wt_offset;
        }

        // -------------------------------------------------------------------
        // Load params
        // Each word contains CONV_W_PER_WORD weight filters packed into the first
        // 63 bits, the last bit is unused. Wts are stored in output-major order.
        // -------------------------------------------------------------------
        for (IdxType m = 0; m < CONVOLVERS; ++m) {
          for (uint32_t kr = 0; kr < K; ++kr) {
            for (uint32_t kc = 0; kc < K; ++kc)
              conv_params[m][kr][kc] = ((wt_word_buffer[m] >> (kr*K+kc))&0x1) == 0 ? 0 : 1;
          }
        }

        DB(3, print_params(conv_params) );
      }

      // -------------------------------------------------------------------
      // Every word in an image
      // -------------------------------------------------------------------
      // Load word
      // (wrd_phase-wrd) is which wrd in the current phase, aligned to img boundary
      if (wrd != words_per_image) {
        for (IdxType m = 0; m < CONVOLVERS; ++m) {
          word_buffer[m] = dmem[dmem_i_idx][m][p*words_per_image + wrd_phase];
        }
      }

      // Compute
      for (IdxType m = 0; m < CONVOLVERS; ++m) {
        if (wrd != words_per_image) {
          // Do the following for each word in an image
          process_word( word_buffer[m], line_buffer[m], conv_params[m],
              conv_out_buffer[m], width_mode, words_per_image, wrd );
        } else {
          // Extra iter after the last word of an image
          conv_halfword( line_buffer[m], conv_params[m], conv_out_buffer[m][0] );
        }
      } // CONVOLVERS

      // -------------------------------------------------------------------
      // Sum results across convolvers
      // -------------------------------------------------------------------
      for (IdxType i = 0; i < WORD_SIZE/2; ++i) {
        // 1st half of conv_out_buffer gets written to 2nd half of wrd-1
        if (wrd == words_per_image || wrd > 0) {
          ConvSum s = 0;
          for (IdxType m = 0; m < CONVOLVERS; ++m)
            s += conv_out_buffer[m][0][i];
          fixed_buffer[wrd_phase-1][1][i] += s;
        }
        // 2nd half of conv_out_buffer gets written to 1st half or wrd
        if (wrd != words_per_image) {
          ConvSum s = 0;
          for (IdxType m = 0; m < CONVOLVERS; ++m)
            s += conv_out_buffer[m][1][i];
          fixed_buffer[wrd_phase][0][i] += s;
        }
      }

      // -------------------------------------------------------------------
      // Increment counters
      // -------------------------------------------------------------------
      if (wrd != words_per_image) {
        wrd++;
        wrd_phase++;
      } else {
        wrd = 0;
      }
    } // wrd_phase = 0 .. WORDS_PER_PHASE

  } // n_phases

  for (uint8_t w = 0; w < words_per_image; ++w) {
    for (uint8_t b = 0; b < WORD_SIZE/2; ++b) {
      fixed_temp[0][b] = fixed_buffer[w][0][b];
      fixed_temp[1][b] = fixed_buffer[w][1][b];
    }

    for (uint8_t i = words_per_image; i < WORDS_PER_PHASE; i += words_per_image) {
      for (uint8_t b = 0; b < WORD_SIZE/2; ++b) {
        fixed_temp[0][b] += fixed_buffer[w+i][0][b];
        fixed_temp[1][b] += fixed_buffer[w+i][1][b];
    } }

    for (uint8_t b = 0; b < WORD_SIZE/2; ++b) {
      fixed_buffer[w][0][b] = fixed_temp[0][b];
      fixed_buffer[w][1][b] = fixed_temp[1][b];
    }
  }

  const Address bank_idx = o_index % CONVOLVERS;
  const Address bank_off = o_index / CONVOLVERS;
  const uint8_t pool_width = 1 << (log_width-1);
  /*if (o_index == 0) {
    unsigned width = 1 << log_width;
    printf ("=== conv result ===\n");
    print_mat(fixed_buffer[0][0], width, 16, width);
  }*/

  NormComp nc = (hi>0) ? -M_INT : M_INT;
  if (ki != 0) nc = -(hi/ki);
  /*if (o_index == 0) {
    printf("  o_idx=%3d: nc=%6d\n", o_index, nc);
  }*/

  static Word outword;
  Word poolword = 0;
  uint8_t lw2_mask = 0;
  for (uint8_t i = 0; i <= log_width-2; ++i) {
    lw2_mask = lw2_mask << 1;
    lw2_mask |= 1;
  }

  for (uint8_t w = 0; w < words_per_image; ++w) {
    Word binword = 0;
    Address o_bank_idx = bank_idx;
    Address o_bank_offset = bank_off*words_per_image + w;
    const uint8_t out_offset = (w % 4) << 4;

    for (uint8_t i = 0; i < WORD_SIZE; ++i) {
      Word temp = (fixed_buffer[w][i / (WORD_SIZE/2)][i % (WORD_SIZE/2)] >= nc) ? 0 : 1;
      temp = temp << i;
      binword |= temp;
    }

    if (norm_mode == 1) {
      outword = binword;
    }
    else if (norm_mode == 2) {
      // horizontal pooling first
      uint32_t poolword_h = 0;
      for (uint8_t i = 0; i < WORD_SIZE/2; ++i) {
        Word temp = (binword >> (2*i)) & (binword >> (2*i+1)) & 0x1;
        temp = temp << i;
        poolword_h |= temp;
      }

      // vertical pooling
      if (w % 4 == 0) poolword = 0;
      for (uint8_t i = 0; i < WORD_SIZE/4; ++i) {
        // source indices
        uint8_t i0 = i >> (log_width-1);
        //i0 = (i0 << log_width) + i(log_width-2,0);
        i0 = (i0 << log_width) + (i & lw2_mask);
        uint8_t i1 = i0 + pool_width;
        // dest index
        uint8_t d0 = out_offset + i;
        Word temp = (poolword_h >> i0) & (poolword_h >> i1) & 0x1;
        temp = temp << d0;
        poolword |= temp;
      }

      // For log_width > 3 we can just assign the word, but log_width = 3 means width = 8,
      // which means pooled width = 4, which is only 16 bits, which is less than 1 Word.
      // So each time we get here we only have 16 bits, meaning we have to accumulate four
      // of these 16-bit batches before writing a word out.
      if (log_width != 3) {
        //dmem[dmem_o_idx][bank_idx][(bank_off*words_per_image + w)/4] = poolword;
        o_bank_offset /= 4;
        outword = poolword;
      } else {
        outword = outword >> WORD_SIZE/4;
        //outword(63,48) = poolword(15,0);
        Word temp = poolword & 0xffff;
        temp = temp << 48;
        outword |= temp;
        //dmem[dmem_o_idx][(o_index/4)%CONVOLVERS][(o_index/4)/CONVOLVERS] = outword;
        o_bank_idx = (o_index/4)%CONVOLVERS;
        o_bank_offset = (o_index/4)/CONVOLVERS;
      }
    }

    dmem[dmem_o_idx][o_bank_idx][o_bank_offset] = outword;
  }
}

void bin_dense(
    const Word wt_mem[CONVOLVERS][C_WT_WORDS],
    const Word kh_mem[KH_WORDS],
    Word dmem[2][CONVOLVERS][C_DMEM_WORDS],
    uint8_t layer_type,
    uint8_t d_i_idx,
    uint8_t d_o_idx,
    const Address o_index,
    const uint32_t n_inputs,
    const uint32_t n_outputs
) {
  assert(layer_type == LAYER_DENSE || n_outputs == 10);
  assert(n_inputs/WORD_SIZE % CONVOLVERS == 0);

  int32_t sum_m[CONVOLVERS];
  // for last layer
  float best_out = -1024;
  Word prediction = -1;

  // read words from dmem and the wt store, dot them
  // o is the output bit, i is the input bit
  for (Address o = 0; o < n_outputs; ++o) {
    const Address o_addr = (o_index+o)/WORD_SIZE;
    const uint32_t o_offset = (o_index+o) % WORD_SIZE;
    Word o_word = dmem[d_o_idx][o_addr%CONVOLVERS][o_addr/CONVOLVERS];

    int32_t sum = 0;

    for (Address i = 0; i < n_inputs; i+=CONVOLVERS*WORD_SIZE) {
      const Address wt_addr = (o*n_inputs+i) / WORD_SIZE;

      for (IdxType j = 0; j < CONVOLVERS; ++j) {
        // in_wrd addr = [(i/WORD_SIZE+j) % CONVOLVERS][(i/WORD_SIZE+j) / CONVOLVERS]
        // wt_wrd addr = [wt_addr % CONVOLVERS][wt_addr / CONVOLVERS]
        const Word in_wrd = dmem[d_i_idx][j][i/WORD_SIZE/CONVOLVERS];
        const Word wt_wrd = wt_mem[j][wt_addr / CONVOLVERS];

        Word x = wt_wrd ^ in_wrd;

        // count_set bit for 64 bits, returns 2*cnt
        x -= (x >> 1) & m1;
        x = (x & m2) + ((x >> 2) & m2);
        x = (x + (x >> 4)) & m4;
        x += x >> 8;
        x += x >> 16;
        x += x >> 32;
        x = x & 0x7f;

        sum_m[j] = WORD_SIZE - (int32_t)(x<<1);
      }

      for (IdxType j = 0; j < CONVOLVERS; ++j)
        sum += sum_m[j];
    } // n_inputs

    // We just hack the KH values in the model
    KType ki;  HType hi;
    load_kh_model(&ki, &hi, kh_mem, 0);
    NormComp nc = (hi>0) ? -M_INT : M_INT;
    if (ki != 0) nc = -(hi/ki);

    // not last layer -> biniarize,
    // otherwise just store the value as a 64bit word
    if (layer_type == LAYER_DENSE) {
      if (o_offset == 0)
        o_word = 0;
      Word mask = (sum < nc) ? 1 : 0;
      mask = mask << o_offset;
      o_word |= mask;
      //printf ("  bit%d> (%4d) %d\n", o_offset, sum, (sum >= nc) ? 0 : 1);
    } else {
      //printf (" >> %d * %f + %f\n", sum.to_int(), ki.to_float(), hi.to_float());
      float out = (float)sum*ki + hi;

      if (o == 0 || out > best_out) {
        prediction = (Word)o;
        best_out = out;
      }
    }

    dmem[d_o_idx][o_addr%CONVOLVERS][o_addr/CONVOLVERS] = o_word;
  } // n_outputs

  // Here we are using o_index as a bit index, not a word index!
  if (layer_type == LAYER_LAST) {
    dmem[d_o_idx][0][0] = prediction;
  }
}


// -----------------------------------------------------------------------
// Accelerator top module
// -----------------------------------------------------------------------
void bnn(
    Word* wt_i,
    Word* kh_i,
    Word* dmem_i,
    Word* dmem_o,
    const Address    n_inputs,
    const Address    n_outputs,
    const Address    input_words,
    const Address    output_words,
    const uint32_t layer_mode,  // [0]='new layer', [2:1]='conv1,conv,dense,last'
    const uint32_t dmem_mode,   // 0 means dmem[0] is input
    const uint32_t width_mode,  // 0=8'b, 1=16'b, 2=32'b
    const uint32_t norm_mode    // 0='do nothing', 1='do norm', 2='do pool'
) {
  DB_PRINT(1, "==== Entering Accel ====\n");
  const uint32_t layer_type = (layer_mode & 0x6) >> 1;
  const uint32_t width = 8 << width_mode;
  DB_PRINT(1, "  Inputs  = %d\n", n_inputs);
  DB_PRINT(1, "  Outputs = %d\n", n_outputs);
  DB_PRINT(1, "  i_words = %d\n", input_words);
  DB_PRINT(1, "  o_words = %d\n", output_words);
  DB_PRINT(1, "  Width = %d\n", width);
  DB_PRINT(1, "  layer_mode = %d %d\n", (layer_mode&0x1)==0 ? 0 : 1, layer_type);
  DB_PRINT(1, "  dmem_mode = %d\n", dmem_mode);

  assert(width <= MAX_WIDTH);
  assert(n_inputs != 0);
  if (layer_type <= LAYER_CONV) {
    assert(input_words % CONVOLVERS == 0);
    assert(n_inputs*width*width <= DMEM_WORDS*WORD_SIZE);
    assert(n_inputs*WT_SIZE <= WT_WORDS*WORD_SIZE);
  }

  static Word dmem[2][CONVOLVERS][C_DMEM_WORDS];
  static Word kh_mem[KH_WORDS];
  static Word wt_mem[CONVOLVERS][C_WT_WORDS];
  static Address kh_index = 0;
  static Address o_index = 0;

  if (layer_mode & 0x1) {
    kh_index = 0;
    o_index = 0;
  } else {
    kh_index = kh_index & 0x1;
  }

  uint8_t dmem_i_idx = dmem_mode;
  uint8_t dmem_o_idx = !dmem_mode;

  // Data input
  const uint8_t words_per_image = 1 << (2*width_mode);
  Address img_idx = 0;  // i / words_per_image;
  IdxType img_off = 0;  // i % words_per_image;
  for (Address i = 0; i < input_words; ++i) {
    if (layer_type == LAYER_CONV) {
      Address bank_idx = img_idx % CONVOLVERS;
      Address bank_off = img_idx / CONVOLVERS;
      dmem[dmem_i_idx][bank_idx][(bank_off<<(2*width_mode)) + img_off] = dmem_i[i];
    }
    else if (layer_type == LAYER_CONV1)
      dmem[dmem_i_idx][i/C_DMEM_WORDS][i%C_DMEM_WORDS] = dmem_i[i];
    else
      dmem[dmem_i_idx][i%CONVOLVERS][i/CONVOLVERS] = dmem_i[i];

    if (++img_off == words_per_image) {
      img_off = 0;
      ++img_idx;
    }
  }

  // Weight input, we must copy every 64-bit Word from the interface
  // into the accelerator
  for (Address i = 0; i < WT_WORDS; ++i) {
    wt_mem[i%CONVOLVERS][i/CONVOLVERS] = wt_i[i];
  }

  if (layer_type == LAYER_CONV1) {
    assert(n_inputs == 3);
    printf("Cannot support first FP conv layer\n");
    exit(1);
  }
  else if (layer_type == LAYER_CONV) {
    assert(norm_mode != 2 || n_outputs % 4 == 0); // needed for pooling of 8x8 image
    assert(n_inputs % CONVOLVERS == 0);

    for (IdxType i = 0; i < n_outputs; ++i) {
      // Load the batch-norm parameters for this output
      KType ki;  HType hi;
      load_kh_model(&ki, &hi, kh_mem, kh_index);

      bin_conv(
          wt_mem,
          ki, hi,
          dmem,
          dmem_i_idx, dmem_o_idx,
          n_inputs,
          o_index,
          i == 0 ? 1 : 0,         // new_batch
          width_mode,
          norm_mode
      );

      kh_index++;
      o_index++;
    }
  }
  else {
    bin_dense(
        wt_mem,
        kh_mem,
        dmem,
        layer_type,
        dmem_i_idx, dmem_o_idx,
        o_index,
        n_inputs, n_outputs
    );

  } // layer_type

  // Data output
  uint32_t words_per_out = words_per_image / ((norm_mode!=2) ? 1 : 4);
  img_idx = 0;
  img_off = 0;
  for (Address i = 0; i < output_words; ++i) {
    // exclude conv6 (width==8, norm_mode==2) here because it writes
    // the output fmaps linearly
    if (layer_type <= LAYER_CONV && !(width_mode == 0 && norm_mode == 2)) {
      Address bank_idx = img_idx % CONVOLVERS;
      Address bank_off = img_idx / CONVOLVERS;
      dmem_o[i] = dmem[dmem_o_idx][bank_idx][bank_off*words_per_out + img_off];
    }
    else
      dmem_o[i] = dmem[dmem_o_idx][i%CONVOLVERS][i/CONVOLVERS];

    if (++img_off == words_per_out) {
      img_off = 0;
      ++img_idx;
    }
  }
}
