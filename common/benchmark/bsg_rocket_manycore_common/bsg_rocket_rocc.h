//this file contains the macros that can be used for issue bsg_rocc commands.
//
#ifndef _BSG_ROCKET_ROCC_H_
#define _BSG_ROCKET_ROCC_H_

#include "bsg_manycore_buffer.h"

#define bsg_rocc_addr_width    32
#define bsg_rocc_cfg_width     1
#define bsg_rocc_x_cord_width 16
#define bsg_rocc_y_cord_width 15

#define bsg_rocc_cfg_shift    ( bsg_rocc_addr_width    )
#define bsg_rocc_x_cord_shift ( bsg_rocc_cfg_width  + bsg_rocc_cfg_shift      )
#define bsg_rocc_y_cord_shift ( bsg_rocc_x_cord_width + bsg_rocc_x_cord_shift )

//the memory size of one tile ( imem + dmem)


//write specific 32bit value into manycore, funct7=0x0
#define bsg_rocc_manycore_addr( y_cord, x_cord, local_addr)     \
                        (   ( (  (uint64_t)(y_cord    ) ) << bsg_rocc_y_cord_shift  )   \
                          | ( (  (uint64_t)(x_cord    ) ) << bsg_rocc_x_cord_shift  )   \
                          | ( (  (uint64_t)(local_addr) ) &  0xFFFFFFFFUL           )   \
                        )

#define bsg_rocc_manycore_cfg_addr( y_cord, x_cord, config_addr)     \
                        (                                                               \
                            ( (        0x1UL            ) << bsg_rocc_cfg_shift     )   \
                          | bsg_rocc_manycore_addr( y_cord, x_cord, config_addr     )   \
                        )

#define bsg_rocc_write( y_cord, x_cord, local_addr, value ) \
  asm volatile ("custom0 x0, %0, %1, 0" : : \
                "r"( bsg_rocc_manycore_addr(y_cord, x_cord, local_addr)),   \
                "r"( value                                             )    \
               )

#define bsg_rocc_config( y_cord, x_cord, config_addr, value ) \
  asm volatile ("custom0 x0, %0, %1, 0" : : \
                "r"( bsg_rocc_manycore_cfg_addr(y_cord, x_cord, config_addr)),   \
                "r"( value                                             )    \
               )

//configure the segment address, funct7=0x1
#define bsg_rocc_seg_addr( addr  )  \
  asm volatile ("custom0 x0, %0, x0, 1" : :"r"( addr ) )

//reset the manycore, funct7=x05
#define bsg_rocc_reset_manycore(  )  \
  asm volatile ("custom0 x0, x0, x0, 0x5;" )

#define bsg_rocc_read( y_cord, x_cord, local_addr, read_value ) \
  asm volatile ("custom0 %[value], %[addr], x0, 0x6; "          \
                                                                \
                :   [value] "=r"( read_value )                  \
                :   [addr]   "r"( bsg_rocc_manycore_addr(y_cord, x_cord, local_addr))   \
               )
//the DMA command
//    eRoCC_core_dma_addr =rocc_instr_funct7_width_gp'(2),
//    eRoCC_core_dma_skip =rocc_instr_funct7_width_gp'(3),
//    eRoCC_core_dma_xfer =rocc_instr_funct7_width_gp'(4),
//    eRoCC_core_dma_fence=rocc_instr_funct7_width_gp'(5)
inline void bsg_rocc_dma_xfer_nb( int *        rocket_addr  ,
                                  int *        manycore_addr,
                                  uint64_t     rocket_skip  ,
                                  uint64_t     manycore_skip,
                                  uint64_t     run_bytes    ,
                                  uint64_t     repeats){

 asm volatile (
    "fence \n"

    "custom0 x0     , %[mc_addr]    , %[rc_addr]    , 2\n"
    "custom0 x0     , %[mc_skip]    , %[rc_skip]    , 3\n"
    "custom0 x0     , %[run_bytes]  , %[repeats]    , 4\n"

    // outputs from the inline assembly block
    :
    // Inputs to the inline assembly block
    : [mc_addr]  "r"(manycore_addr),
      [rc_addr]  "r"(rocket_addr  ),
      [mc_skip]  "r"(manycore_skip),
      [rc_skip]  "r"(rocket_skip  ),
      [run_bytes]"r"(run_bytes    ),
      [repeats]  "r"(repeats      )
    // Tell the compiler this inline assembly block changes memory
    : "memory"
    );
}

//#define bsg_rocc_dma_fence( run_bytes  )  \
//  asm volatile ("custom0 %[run_bytes], x0, x0, 5" :[run_bytes] "=r"( run_bytes ) )

#define bsg_rocket_fence()  asm volatile ("fence \n")

#define bsg_rocc_unfreeze(y_cord, x_cord) bsg_rocc_config( (y_cord), (x_cord), 0x0, 0x0)
#define bsg_rocc_freeze(y_cord, x_cord) bsg_rocc_config( (y_cord), (x_cord), 0x0, 0x1)


//Here is the memory content should be moved into manycore !
//The definition and initial value will be automatically generated from manycore program
extern int manycore_mem_vect[] ;
extern int bsg_rocc_manycore_mem_words ;
/////////////////////////////////////////////
void bsg_rocc_load_manycore(int y_cord, int x_cord );
void bsg_rocc_load_manycore_len(int y_cord, int x_cord, int word_len );
// using a single DMA command to load a single  manycore images.
void bsg_rocc_dma_load_manycore_nb( int y_cord, int x_cord);
// using a single DMA command to load multiple manycore images.
void bsg_rocc_dma_load_x_manycore_nb( int y_cord      ,
                                      int x_cord_start, int x_cord_end);
int  bsg_rocc_poll_task( volatile manycore_task_s *pRocketViewTask, int waiting_cycle_limit);
int  bsg_rocc_poll_task_N( int N, volatile manycore_task_s *pRocketViewTask, int waiting_cycle_limit);
#endif
