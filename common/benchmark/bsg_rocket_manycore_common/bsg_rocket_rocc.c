#include <stdint.h>
#include "bsg_manycore_buffer.h"
#include "bsg_rocket_rocc.h"

//load the memory content into the manycore tile( y_cord, x_cord)
//the IMEM_INIT_LEN is defined in Makefile.
//the DMEM_INIT_LEN is defined in Makefile.
void bsg_rocc_load_manycore(int y_cord, int x_cord ){
    int i;
    int DM_START_ADDR= ( 0x1000 >> 2 );
    for( i=0; i< ( IMEM_INIT_LEN >>2 ); i++)
        bsg_rocc_write( y_cord, x_cord, i*4,  manycore_mem_vect[i]);

    for( i=DM_START_ADDR; i< DM_START_ADDR + (IMEM_INIT_LEN>>2); i++)
        bsg_rocc_write( y_cord, x_cord, i*4,  manycore_mem_vect[i]);
}

//load part of the memory content into the manycore tile( y_cord, x_cord)
void bsg_rocc_load_manycore_len(int y_cord, int x_cord, int word_len ){
    int i;
    for( i=0; i< word_len ; i++)
        bsg_rocc_write( y_cord, x_cord, i*4,  manycore_mem_vect[i]);
}
//load the memory content into the manycore tile( y_cord, x_cord)
//using dma functions.
//this is a non-block function, user has to issue fence instuction to check the completion
void bsg_rocc_dma_load_manycore_nb(int y_cord, int x_cord ){

int * rocket_addr   = manycore_mem_vect ;
int * manycore_addr = (int *) bsg_rocc_manycore_addr( y_cord, x_cord, 0x0);
bsg_rocc_dma_xfer_nb( rocket_addr                       ,
                      manycore_addr                     ,
                      0                                 ,
                      0,
                      (bsg_rocc_manycore_mem_words*4)   ,
                      1);
}

//load the memory content into the multile manycore tiles
void bsg_rocc_dma_load_x_manycore_nb(int y_cord      ,
                                     int x_cord_start, int x_cord_end ){

int * rocket_addr   = manycore_mem_vect ;
int * manycore_addr = (int *) bsg_rocc_manycore_addr( y_cord, x_cord_start, 0x0);

int         x_tiles_num   = x_cord_end - x_cord_start + 1;
int64_t    mem_byte_size = bsg_rocc_manycore_mem_words * 0x4UL                 ;
int64_t    mem_x_distance= ( 0x1UL << bsg_rocc_x_cord_shift  ) - mem_byte_size  ;
bsg_rocc_dma_xfer_nb( rocket_addr                           ,
                      manycore_addr                         ,
                      -mem_byte_size                        ,
                      mem_x_distance                        ,
                      mem_byte_size                         ,
                      x_tiles_num                           );
}

//wait the task done.
int  bsg_rocc_poll_task( volatile manycore_task_s *pRocketViewTask, int waiting_cycle_limit) {
    int volatile *pDone = (int volatile * )( &( pRocketViewTask-> done) );
    int i=0;
    do {
        asm volatile( "fence ");
        i++;
      }while( (i< waiting_cycle_limit ) & ( *pDone == 0x0) );

    return i;
}

int  bsg_rocc_poll_task_N( int N, volatile manycore_task_s *pRocketViewTask, int waiting_cycle_limit) {
    int volatile *pDone = (int volatile * )( &( pRocketViewTask-> done) );
    int i=0;
    int need_wait;

    do {

        int need_wait = 0 ;
        for( int j=0; j< N; j++) {
            if( pDone[j] == 0x0 ) {
                need_wait == 1;
            }
        }

        asm volatile( "fence ");
        i++;

      }while( ( i< waiting_cycle_limit ) && ( need_wait == 1) );

    return i;
}
