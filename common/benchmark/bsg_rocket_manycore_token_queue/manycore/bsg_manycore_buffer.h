// This file contains the variable to be used both in manycore and rocket.
#ifndef  _MC_RC_BUFFER_H_
#define  _MC_RC_BUFFER_H_

#ifndef MANYCORE_PROG
#define _MC_RC_PREFIX_ extern volatile

#else
#define _MC_RC_PREFIX_ 

#endif

///////////////////////////////////////////////////////////////
#ifndef MANYCORE_SRC_BUF_LEN
    #define MANYCORE_SRC_BUF_LEN  8
#endif

#ifndef MANYCORE_DST_BUF_LEN
    #define MANYCORE_DST_BUF_LEN  8
#endif

typedef struct manycore_task_def {
    //the base address that this struct has been mapped.
    //When manycore try to store data back to the rocket, it 
    //should added the base_addr to the buffer to get the right 
    //address
    unsigned int base_addr;
    int source[ MANYCORE_SRC_BUF_LEN ];
    int done                          ;
    int result[ MANYCORE_DST_BUF_LEN ];
} manycore_task_s;

///////////////////////////////////////////////////////////////
//
//     GLOBAL DATA STRUCTURE DECLARATION
//     ----------------------------------
_MC_RC_PREFIX_  manycore_task_s  manycore_data_s;

///////////////////////////////////////////////////////////////
// Routines can only be used in manycore programs.
#ifdef MANYCORE_PROG
#include "bsg_manycore.h"

inline manycore_task_s * bsg_rocket_view_task( manycore_task_s * pMCViewTask){
    return (manycore_task_s *)( (unsigned int)pMCViewTask + pMCViewTask->base_addr);
} 

inline void bsg_rocc_finish( manycore_task_s *pMCViewTask){
    manycore_task_s *pRocketViewTask = bsg_rocket_view_task( pMCViewTask);
    bsg_remote_ptr_io_store(bsg_active_rocc_index, &(pRocketViewTask->done), 0x1);
}


#else

///////////////////////////////////////////////////////////////
// Routines can only be used in rocket programs.
inline manycore_task_s * bsg_rocket_view_task( volatile manycore_task_s * pMCViewTask,       \
                                               unsigned int    * manycore_mem_vect){
  return ( manycore_task_s*) (  (uint64_t)(&manycore_data_s)  \
                              + (uint64_t)(manycore_mem_vect) \
                             );
}
#endif

#endif
