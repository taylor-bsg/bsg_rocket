//====================================================================
// bsg_rocket_manycore_token_queue.c
// 02/08/2016, shawnless.xie@gmail.com
//====================================================================
// The following is a basic RISC-V program to test the functionality of the
// Rocket + manycore, in which manycore just copy data back to Rocket

#include <assert.h>
#include <stdio.h>
#include <stdint.h>

#include "bsg_manycore_buffer.h"
#include "bsg_rocket_rocc.h"
#include "manycore.cfg.h"

#define DMA_LOAD
////////////////////////////////////////////////////////////
//
int   waiting_cycle_limit = 100000;

void init_source( int y_cord, int x_cord, \
            volatile manycore_task_s * pMCViewTask,  \
            volatile manycore_task_s * pRocketViewTask);

int check_result(volatile manycore_task_s *pRocketViewTask);

/////////////////////////////////////////////////////////////
int main() {
  int i=0;
  volatile manycore_task_s * pRocketViewTask =bsg_rocket_view_task( & manycore_data_s, \
                                                                      manycore_mem_vect); 

  //setup the segment address
  bsg_rocc_seg_addr(  manycore_mem_vect );
  
  #ifndef DMA_LOAD 
    bsg_rocc_load_manycore(0, 0);
    bsg_rocc_load_manycore(0, 1);
  #else
    bsg_rocc_dma_load_x_manycore_nb(0, 0, 1);
    bsg_rocket_fence( );
  #endif
  printf(" ==> Finish loading \n" );

  init_source(0,1, &manycore_data_s, pRocketViewTask);
  init_source(0,0, &manycore_data_s, pRocketViewTask);

  bsg_rocc_unfreeze( 0x0, 0x0);
  bsg_rocc_unfreeze( 0x0, 0x1);

  i = bsg_rocc_poll_task( pRocketViewTask, waiting_cycle_limit );

  if( i == waiting_cycle_limit ) 
    printf(" ==> FAIL ! wait %d but not result return \n", waiting_cycle_limit );
  else if( check_result( pRocketViewTask ) ) 
    printf(" ==> FAIL ! received data is not correct \n");
  else
    printf(" ==> PASS !\n");

 return 0;
}

/////////////////////////////////////////////////////////////
//
int buffer[kBufferSize+10];

int expect[kBlocks+10];

int source_process(int *ptr) {
  for (int j = 0; j < kTransmitSize; j+=2) {
    ptr[j]   = j;
    ptr[j+1] = j;
  }
}

int dest_process(int sum, int *ptr, volatile int *io_ptr) {
  for (int i = 0; i < kTransmitSize; i++)
    sum += *ptr++;
  return sum;
}

int get_expect_result() {
  int sum;
  int io_ptr;

  //get the first result
  source_process(buffer);
  expect[0]=dest_process(0,buffer,&io_ptr);

  //get the remaining result
  for (int i = 1; i < kBlocks; i++) {
    source_process(buffer);
    expect[i]=dest_process(expect[i-1],buffer,&io_ptr);
  }
}

//init the source data
void init_source( int y_cord, int x_cord, \
            volatile manycore_task_s * pMCViewTask,  \
            volatile manycore_task_s * pRocketViewTask){
  //setup the base address
  bsg_rocc_write( y_cord, x_cord, &(pMCViewTask->base_addr), manycore_mem_vect);

  //reset the done signal.
  pRocketViewTask->done = 0;
}

//check the result 
int check_result(volatile manycore_task_s *pRocketViewTask){

    get_expect_result();

    for( int i=0; i< kBlocks ; i ++ ){
        if( pRocketViewTask->result[i] != expect[i] ) {
            printf(" ==> FAIL@[%d], result=%08x, expect=%08x\n", i, pRocketViewTask->result[i], expect[i] );
            return 1;
        }
    }
    return 0;
}
