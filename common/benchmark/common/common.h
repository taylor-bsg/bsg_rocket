//========================================================================
// common
//========================================================================
// Helper routines for writing PARCv2 microbenchmarks.

#ifndef COMMON_H
#define COMMON_H

#include "common-misc.h"
#if defined(_RISCV) && !defined(_ROCKET)
#include "common-wprint.h"
#endif

#endif /* COMMON_H */

