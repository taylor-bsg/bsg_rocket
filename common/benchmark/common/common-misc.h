//========================================================================
// common-misc
//========================================================================

#ifndef COMMON_MISC_H
#define COMMON_MISC_H

#if (defined(_RISCV) && defined(_ROCKET)) || !defined(_RISCV)
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#endif

#if defined(_RISCV) && defined(_ROCKET) && defined(_PK)
#include <riscv-pk/encoding.h>
#endif

#if defined(_RISCV) && defined(_ROCKET) && !defined(_PK)
#include "util.h"
#endif

#if (defined(_RISCV) && defined(_ROCKET) && defined(_PK)) || !defined(_RISCV)
#include <assert.h>
#endif

#if defined(_RISCV) && defined(_ROCKET)
static unsigned long __global_cycles_;
static unsigned long __global_insts_ ;
#endif

//------------------------------------------------------------------------
// Typedefs
//------------------------------------------------------------------------

typedef unsigned char byte;
typedef unsigned int  uint;
#if defined(_RISCV) && (!defined(_ROCKET) || (defined(_ROCKET) && !defined(_PK)))
//Type
typedef          char bool;

// Values
#define false 0
#define true  1
#endif

//------------------------------------------------------------------------
// Force pagefault
//------------------------------------------------------------------------
#if defined(_RISCV) && defined(_ROCKET) && defined(_PK)

#define LOAD_ARRAY(array, _max, element_sz) {          \
  uint32_t max = _max * element_sz;                    \
  unsigned char *a = (unsigned char *)array;           \
  for(uint32_t i = 0; i < (max); i += (4 * 1024)) {    \
    asm volatile                                       \
    (                                                  \
      "lb zero, %[mem_addr]\n"                         \
      :                                                \
      : [mem_addr] "m"(a[i])                           \
      : "memory"                                       \
    );                                                 \
  }                                                    \
  asm volatile                                         \
  (                                                    \
    "lb zero, %[mem_addr]\n"                           \
    :                                                  \
    : [mem_addr] "m"(a[(max) - 1])                     \
    : "memory"                                         \
  );                                                   \
}

#define LOAD_STATIC_ARRAY(a)                           \
  LOAD_ARRAY(a, sizeof(a)/sizeof(a[0]), sizeof(a[0]));

#else

#define LOAD_ARRAY(a, max, element_sz)
#define LOAD_STATIC_ARRAY(a)

#endif

//------------------------------------------------------------------------
// Argument Parser
//------------------------------------------------------------------------

#define START_ARGS_PARSER                      \
  uint32_t maxLen = 0;                         \
  char *__arg;                                 \
  char *__str;                                 \

#define CHECK_ARGS_INT(args_str, var) {        \
  maxLen = strlen(argv[i]);                    \
  __str = argv[i] + sizeof(args_str);          \
  __arg = (char *)args_str;                    \
  if (maxLen >= sizeof(args_str) + 1)          \
  {                                            \
    maxLen = sizeof(args_str) - 1;             \
    if(strncmp(__arg, argv[i], maxLen) == 0) { \
      var = atoi(__str);                       \
      continue;                                \
    }                                          \
  }                                            \
}

#define CHECK_ARGS_BOOL(args_str, var) {       \
  maxLen = strlen(argv[i]);                    \
  __arg = (char *)args_str;                    \
  if (maxLen == sizeof(args_str) - 1)          \
  {                                            \
    maxLen = sizeof(args_str) - 1;             \
    if(strncmp(__arg, argv[i], maxLen) == 0) { \
      var = true;                              \
      continue;                                \
    }                                          \
  }                                            \
}

//------------------------------------------------------------------------
// exit
//------------------------------------------------------------------------
// exit the program with the given status code

#if defined(_RISCV) && !defined(_ROCKET)

inline
static void exit( int i )
{

  int msg = 0x00010000 | i;

  asm ( "csrw 0x7C0, %0;" :: "r"(msg) );
}

#endif

//------------------------------------------------------------------------
// Asserts
//------------------------------------------------------------------------

#if defined(_RISCV) && (!defined(_ROCKET) || (defined(_ROCKET) && !defined(_PK)))

#define _stringfy(x) #x
#define stringfy(x) _stringfy(x)

#define assert(x) {                                    \
  _assert(x, stringfy(x), (char*)__FILE__,             \
                                 __LINE__,             \
                          (char*)__PRETTY_FUNCTION__); \
}

inline void _assert(uint32_t cond, char *str, char *file, uint32_t ln, char *func)
{
  if(!cond) {
    printf("assertion \"%s\" failed: file \"%s\", line %d, function: %s\n", str, file, ln, func);
    exit(ln + 1);
  }
}
#endif

//------------------------------------------------------------------------
// test_fail
//------------------------------------------------------------------------

#if defined(_RISCV) && !defined(_ROCKET)

inline
static void test_fail( int index, int val, int ref )
{

  int status = 0x00020001;
  asm( "csrw 0x7C0, %0;"
       "csrw 0x7C0, %1;"
       "csrw 0x7C0, %2;"
       "csrw 0x7C0, %3;"
       :
       : "r" (status), "r" (index), "r" (val), "r" (ref)
  );

}

#else

inline
static void test_fail( int index, int val, int ref )
{
  printf( "\n" );
  printf( "  [ FAILED ] dest[%d] != ref[%d] (%d != %d)\n",
                          index, index, val, ref );
  printf( "\n" );
#ifdef _ROCKET
  assert(0);
#else
  exit(1);
#endif
}

#endif

//------------------------------------------------------------------------
// test_pass
//------------------------------------------------------------------------

inline
static void test_pass()
{
#if defined(_RISCV) && !defined(_ROCKET)

  int status = 0x00020000;
  asm( "csrw 0x7C0, %0;"
       :
       : "r" (status)
  );

#else

  printf( "\n" );
  printf( "  [ passed ] \n" );
  printf( "\n" );
#ifndef _ROCKET
  exit(0);
#endif

#endif
}

//------------------------------------------------------------------------
// Enabling stats
//------------------------------------------------------------------------

#if defined(_RISCV) && defined(_ROCKET) && defined(_PK)

inline static void setStats(int enable)
{
  write_csr(stats, enable);
}

#endif

//------------------------------------------------------------------------
// test_stats_on
//------------------------------------------------------------------------

inline
static void test_stats_on()
{

#if defined(_RISCV) && !defined(_ROCKET)

  int status = 1;
  asm( "csrw 0x7C1, %0;"
       :
       : "r" (status)
  );

#elif defined(_RISCV) && defined(_ROCKET)

  int tmp;

  // Warm up counters
  asm volatile (
    "lw  %[tmp], %[cycles_counter]\n"
    "lw  %[tmp], %[insts_counter] \n"
    "and %[tmp], %[tmp], %[tmp]   \n"
    : [tmp]            "=r" (tmp)
    : [cycles_counter]  "m" (__global_cycles_),
      [insts_counter]   "m" (__global_insts_)
    : "memory"
  );

  setStats(1);

  __global_cycles_ = -rdcycle  ();
  __global_insts_  = -rdinstret();

#endif

}

//------------------------------------------------------------------------
// test_stats_off
//------------------------------------------------------------------------

inline
static void test_stats_off()
{

#if defined(_RISCV) && !defined(_ROCKET)

  int status = 0;
  asm( "csrw 0x7C1, %0;"
       :
       : "r" (status)
  );

#elif defined(_RISCV) && defined(_ROCKET)

  __global_cycles_ += rdcycle();
  __global_insts_  += rdinstret();

  setStats(0);

#if defined(_PK)

  printf("\n\tNumber of Cycles : % 12ld cycles\n"
           "\tNumber of Insts  : % 12ld insts \n"
           "\n"
           , __global_cycles_, __global_insts_ );

#else

  printf("\n\tNumber of Cycles : %012ld cycles\n"
           "\tNumber of Insts  : %012ld insts \n"
           "\n"
           , __global_cycles_, __global_insts_ );

#endif

#endif

}

#endif /* COMMON_MISC_H */
