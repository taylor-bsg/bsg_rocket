//========================================================================
// ubmark-memcpy-defs.h
//========================================================================

#include <stdint.h>

#ifndef __MEMCPY_DEFS_H
#define __MEMCPY_DEFS_H

// Type defs
typedef uint64_t word_t;

// Constants
#define PAGE_SIZE (1 << 12)

#endif //__MEMCPY_DEFS_H
