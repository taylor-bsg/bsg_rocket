#=======================================================================
# UCB CS250 Makefile fragment for benchmarks
#-----------------------------------------------------------------------
#
# Each benchmark directory should have its own fragment which
# essentially lists what the source files are and how to link them
# into an riscv and/or host executable. All variables should include
# the benchmark name as a prefix so that they are unique.
#
####################################################################
# Rewrite some options in the to Makefile
RISCV_LINK_OPTS += -Wl,--just-symbols=$(bmarks)/manycore.riscv64,-Map=$(bmarks).map
incs += -I./$(bmarks)/manycore -I./bsg_rocket_manycore_common
VPATH +=bsg_rocket_manycore_common
####################################################################

$(bmarks)_c_src = \
	$(bmarks).c \
	manycore.vec.c	\
	syscalls.c \
	bsg_rocket_rocc.c

$(bmarks)_riscv_src = \
	crt.S \

$(bmarks)_c_objs     = $(patsubst %.c, %.o, $($(bmarks)_c_src))
$(bmarks)_riscv_objs = $(patsubst %.S, %.o, $($(bmarks)_riscv_src))

$(bmarks)_host_bin = $(bmarks).host
$($(bmarks)_host_bin) : $($(bmarks)_c_src)
	$(HOST_COMP) $^ -o $($(bmarks)_host_bin)

$(bmarks)_riscv_bin = $(bmarks).riscv
$($(bmarks)_riscv_bin) : $($(bmarks)_c_objs) $($(bmarks)_riscv_objs)
	$(RISCV_LINK) $($(bmarks)_c_objs) $($(bmarks)_riscv_objs) -o $($(bmarks)_riscv_bin) $(RISCV_LINK_OPTS)

junk += $($(bmarks)_c_objs) $($(bmarks)_riscv_objs) \
        $($(bmarks)_host_bin) $($(bmarks)_riscv_bin)
