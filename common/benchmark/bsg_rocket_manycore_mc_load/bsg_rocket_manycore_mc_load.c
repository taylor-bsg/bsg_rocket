//====================================================================
// bsg_rocket_manycore_loopback.c
// 02/08/2016, shawnless.xie@gmail.com
//====================================================================
// The following is a basic RISC-V program to test the functionality of the
// Rocket + manycore, in which manycore just copy data back to Rocket

#include <assert.h>
#include <stdio.h>
#include <stdint.h>

#include "bsg_manycore_buffer.h"
#include "bsg_rocket_rocc.h"

//#define DMA_LOAD
////////////////////////////////////////////////////////////
//
int   waiting_cycle_limit = 10000;

void init_source( int y_cord, int x_cord, \
            volatile manycore_task_s * pMCViewTask,  \
            volatile manycore_task_s * pRocketViewTask);

int check_result(volatile manycore_task_s *pRocketViewTask);

/////////////////////////////////////////////////////////////
int main() {
  int i=0;
  volatile manycore_task_s * pRocketViewTask = \
        ( volatile manycore_task_s*) (   (uint64_t)(&manycore_data_s)  \
                                       + (uint64_t)(manycore_mem_vect) \
                                     );

  //setup the segment address
  bsg_rocc_seg_addr(  manycore_mem_vect );

  //reset the manycore
  bsg_rocc_reset_manycore();

  //load the manycore image.
  #ifndef DMA_LOAD
    bsg_rocc_load_manycore(0, 0);
  #else
    bsg_rocc_dma_load_manycore_nb(0, 0);
    bsg_rocket_fence( );
  #endif

  //initial the source data for manycore
  init_source(0,0, &manycore_data_s, pRocketViewTask);

  //start the manycore
  bsg_rocc_unfreeze( 0x0, 0x0);

  //wait the result
  i = bsg_rocc_poll_task( pRocketViewTask, waiting_cycle_limit );

  if( i == waiting_cycle_limit )
    printf(" ==> FAIL ! wait %d but not result return \n", waiting_cycle_limit );
  else if( check_result( pRocketViewTask ) )
    printf(" ==> FAIL ! received data is not correct \n");
  else
    printf(" ==> PASS !\n");

 return 0;
}

/////////////////////////////////////////////////////////////
//init the source data
void init_source( int y_cord, int x_cord, \
            volatile manycore_task_s * pMCViewTask,  \
            volatile manycore_task_s * pRocketViewTask){
  int i;
  for(i=0; i< MANYCORE_SRC_BUF_LEN; i++)
        pRocketViewTask->source[i] =  i;

  //setup the base address
  bsg_rocc_write( y_cord, x_cord, &(pMCViewTask->base_addr), manycore_mem_vect);

  //reset the done signal.
  pRocketViewTask->done = 0;
}

//check the result
int check_result(volatile manycore_task_s *pRocketViewTask){
    int i=0;
    for( i=0; i< MANYCORE_DST_BUF_LEN ; i ++ ){
        if( pRocketViewTask->result[i] != i ) return 1;
    }
    return 0;
}
