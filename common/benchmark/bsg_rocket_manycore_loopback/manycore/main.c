
#define MANYCORE_PROG
#include "bsg_manycore.h"

#include "bsg_manycore_buffer.h"

//this macro will defined in Makefile, indicates which rocc can be wrote into
//#define bsg_active_rocc_index
int main()
{
  int i;
  int rocket_address=0;

  for(i=0; i< MANYCORE_DST_BUF_LEN; i++){
    rocket_address= ( unsigned int )manycore_data_s.result + i*4 + manycore_data_s.base_addr;
    bsg_remote_ptr_io_store(bsg_active_rocc_index, ( rocket_address ), manycore_data_s.source[i]);
  }
  //write the done signal
  rocket_address= ( unsigned int )(&manycore_data_s.done)  + manycore_data_s.base_addr;
  bsg_remote_ptr_io_store(bsg_active_rocc_index, (rocket_address) , 0x1);

  bsg_wait_while(1);
}

////////////////////////////////////////////////////////////////
//Print the current manycore configurations
#pragma message (bsg_VAR_NAME_VALUE( bsg_tiles_X )  )
#pragma message (bsg_VAR_NAME_VALUE( bsg_tiles_Y )  )
