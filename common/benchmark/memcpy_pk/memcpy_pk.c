//========================================================================
// ubmark-memcpy
//========================================================================

#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <assert.h>

#include "memcpy_defs.h"
#include "memcpy_data.h"

//------------------------------------------------------------------------
// Calling accelerator
//------------------------------------------------------------------------
void memcpy_aux(word_t *data_in, word_t volatile *data_out, uint32_t length)
{
  for(uint32_t i = 0; i < length; i++)
  {
    data_out[i] = data_in[i];
  }
}

//------------------------------------------------------------------------
// Test Harness
//------------------------------------------------------------------------
int main() {

  word_t          *data_in;
  word_t volatile *data_out;

  // Load the input data
  printf ("## Setting input data\n");
  data_in = (word_t *)data_random;

  // Allocating Output buffer!
  printf ("## Allocating output buffers\n");
  data_out = (word_t volatile *)malloc(DATA_SIZE * sizeof(word_t));

  assert(data_out);

  // Invoking memcpy
  memcpy_aux(data_in, data_out, DATA_SIZE);

  // Checking
  uint32_t failed = 0;
  for(uint32_t i = 0; i < DATA_SIZE; i++)
  {
    if(data_in[i] != data_out[i])
    {
      failed = i + 1;

      break;
    }
  }

  if(failed)
  {
    printf("Test failed!\n");
    printf("  first mismatch @ %d\n", failed - 1);
    printf("  %lu bytes in\n", (failed - 1) * sizeof(word_t));
  }
  else
  {
    printf ("Test passed!\n");
  }

  return failed;
}

