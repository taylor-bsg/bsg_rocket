#=======================================================================
# UCB CS250 Makefile fragment for benchmarks
#-----------------------------------------------------------------------
#
# Each benchmark directory should have its own fragment which
# essentially lists what the source files are and how to link them
# into an riscv and/or host executable. All variables should include
# the benchmark name as a prefix so that they are unique.
#

memcpy_pk_c_src = \
	memcpy_pk.c \
	syscalls.c  \

memcpy_pk_riscv_src = \
	crt.S \

memcpy_pk_c_objs     = $(patsubst %.c, %.o, $(memcpy_pk_c_src))
memcpy_pk_riscv_objs = $(patsubst %.S, %.o, $(memcpy_pk_riscv_src))

memcpy_pk_host_bin = memcpy_pk.host
$(memcpy_pk_host_bin) : $(memcpy_pk_c_src)
	$(HOST_COMP) $^ -o $(memcpy_pk_host_bin)

memcpy_pk_riscv_bin = memcpy_pk.riscv
$(memcpy_pk_riscv_bin) : $(memcpy_pk_c_objs) $(memcpy_pk_riscv_objs)
	$(RISCV_LINK) $(memcpy_pk_c_objs) $(memcpy_pk_riscv_objs) -o $(memcpy_pk_riscv_bin) $(RISCV_LINK_OPTS)

junk += $(memcpy_pk_c_objs) $(memcpy_pk_riscv_objs) \
        $(memcpy_pk_host_bin) $(memcpy_pk_riscv_bin)
