//========================================================================
// ubmark-memcpy-xcel-loopback
//========================================================================
// Loopback test for BNN. This writes to some registers in BNN and reads
// them back to confirm that the accelerator exists.

#include <stdlib.h>
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>

// Input data
uint64_t messages [][4] = {

  // Zero all registers
  { 1,          0, 1,          0},
  { 2,          0, 1,          0},
  { 3,          0, 1,          0},

  // Read zero'd registers
  { 1,          0, 0,          0},
  { 2,          0, 0,          0},
  { 3,          0, 0,          0},

  // Write random data
  { 1, 0xcf0c17fa, 1, 0         },
  { 2, 0x246220aa, 1, 0         },
  { 3, 0x278339bd, 1, 0         },

  // Read random data
  { 1,          0, 0, 0xcf0c17fa},
  { 2,          0, 0, 0x246220aa},
  { 3,          0, 0, 0x278339bd},

};

#define NUMBER_OF_INPUTS (sizeof(messages) / sizeof(uint64_t) / 4)

// Helper method to write and read from the accelerator
uint64_t xcelreq(uint32_t dest, uint64_t val, uint32_t wr)
{
  uint64_t reg = 0;

  if(wr)
  {
    switch (dest)
    {
      case 0:
        asm volatile (
          "custom0 0, %[value], 0, 1\n"

          :
          : [value] "r"(val)
          : "memory"
        );
        break;

      case 1:
        asm volatile (
          "custom0 1, %[value], 0, 1\n"

          :
          : [value] "r"(val)
          : "memory"
        );
        break;

      case 2:
        asm volatile (
          "custom0 2, %[value], 0, 1\n"

          :
          : [value] "r"(val)
          : "memory"
        );
        break;
 
      case 3:
        asm volatile (
          "custom0 3, %[value], 0, 1\n"

          :
          : [value] "r"(val)
          : "memory"
        );
        break;
    }
  }
  else
  {
    switch (dest)
    {
      case 0:
        asm volatile (
          "custom0 %[reg] , 0, 0, 0\n"

          : [reg]  "=r"(reg)
          :
          : "memory"
        );

        break;

      case 1:
        asm volatile (
          "custom0 %[reg] , 0, 1, 0\n"

          : [reg]  "=r"(reg)
          :
          : "memory"
        );

        break;

      case 2:
        asm volatile (
          "custom0 %[reg] , 0, 2, 0\n"

          : [reg]  "=r"(reg)
          :
          : "memory"
        );

        break;

      case 3:
        asm volatile (
          "custom0 %[reg] , 0, 3, 0\n"

          : [reg]  "=r"(reg)
          :
          : "memory"
        );

        break;
    }
  }

  return reg;
}

int main()
{
  // Iterate through an array of input messages and compare to expected output
  printf("Running loopback for %u message(s).\n\n", (unsigned int)NUMBER_OF_INPUTS);

  for(uint32_t i = 0; i < NUMBER_OF_INPUTS; i++)
  {
    // Send message
    uint64_t ret = xcelreq(messages[i][0], messages[i][1], messages[i][2] != 0);

    if(ret != messages[i][3])
    {
      printf("\n\t[ TEST FAILED ]");
      printf("\n\n\tERROR: Failed at message %d.", i);
      printf("\n\tReturned value of 0x%" PRIx64 " does not match expected value of 0x%" PRIx64 ".\n\n", ret, messages[i][3]);
      return (i + 1);
    }
  }

  printf("\n\t[ TEST PASSED ]\n\n");
  return 0;
}
