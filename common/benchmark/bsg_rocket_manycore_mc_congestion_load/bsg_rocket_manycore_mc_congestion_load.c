//====================================================================
// bsg_rocket_manycore_loopback.c
// 02/08/2016, shawnless.xie@gmail.com
//====================================================================
// The following is a basic RISC-V program to test the functionality of the
// Rocket + manycore, in which manycore just copy data back to Rocket

#include <assert.h>
#include <stdio.h>
#include <stdint.h>

#include "bsg_manycore_buffer.h"
#include "bsg_rocket_rocc.h"


#define COMP_RESULT(i, id) ( (i)|( (id)<<16 ) )

//#define DMA_LOAD
////////////////////////////////////////////////////////////
//
int   waiting_cycle_limit = 10000;

void init_source( int y_cord, int x_cord, \
            volatile manycore_task_s * pMCViewTask,  \
            volatile manycore_task_s * pRocketViewTask);

int check_result( int y_cord, int _x_cord, volatile manycore_task_s *pRocketViewTask);

/////////////////////////////////////////////////////////////
int main() {
  int i=0;
  int x_cord,y_cord;
  volatile manycore_task_s * pRocketViewTask = \
        ( volatile manycore_task_s*) (   (uint64_t)(&manycore_data_s)  \
                                       + (uint64_t)(manycore_mem_vect) \
                                     );

  //setup the segment address
  bsg_rocc_seg_addr(  manycore_mem_vect );

  //reset the manycore
  bsg_rocc_reset_manycore();

  //load the manycore image.
  for(y_cord=0; y_cord < Y_TILES; y_cord++) {
      for(x_cord=0; x_cord < X_TILES; x_cord++) {
            bsg_rocc_load_manycore(y_cord, x_cord);
        }
  }

  //initial the source data for manycore
  for(y_cord=0; y_cord < Y_TILES; y_cord++) {
      for(x_cord=0; x_cord < X_TILES; x_cord++) {
            init_source(y_cord, x_cord, &manycore_data_s, pRocketViewTask);
        }
  }

  //start the manycore
  for(y_cord=0; y_cord < Y_TILES; y_cord++) {
      for(x_cord=0; x_cord < X_TILES; x_cord++) {
            bsg_rocc_unfreeze( y_cord, x_cord);
        }
  }

  //wait the result
  i = bsg_rocc_poll_task_N( T_TILES, pRocketViewTask, waiting_cycle_limit );

  if( i >= waiting_cycle_limit )
    printf(" ==> FAIL ! wait %d but not result return \n", waiting_cycle_limit );
  else{

    for(y_cord=0; y_cord < Y_TILES; y_cord++) {
      for(x_cord=0; x_cord < X_TILES; x_cord++) {
            if( check_result( y_cord, x_cord, pRocketViewTask ) )
                printf(" (y:%d,x:%d)==> FAIL ! received data is not correct \n", y_cord, x_cord);
            else
                printf(" (y:%d,x:%d)==> PASS !\n", y_cord, x_cord);
        }
    }
  }

 return 0;
}

/////////////////////////////////////////////////////////////
//init the source data
void init_source( int y_cord, int x_cord, \
            volatile manycore_task_s * pMCViewTask,  \
            volatile manycore_task_s * pRocketViewTask){
  int i;
  int tile_id = TILE_ID( y_cord, x_cord );
  for(i=0; i< MANYCORE_SRC_BUF_LEN; i++)
        pRocketViewTask->source [tile_id] [i] = COMP_RESULT(i, tile_id);

  //setup the base address
  bsg_rocc_write( y_cord, x_cord, &(pMCViewTask->base_addr), manycore_mem_vect);

  //write the id data
  bsg_rocc_write( y_cord, x_cord, &(pMCViewTask->tile_id), tile_id);

  //reset the done signal.
  pRocketViewTask->done[tile_id] = 0;

}

//check the result
int check_result(int y_cord, int x_cord, volatile manycore_task_s *pRocketViewTask){
    int i=0;
    int tile_id = TILE_ID( y_cord, x_cord );

    for( i=0; i< MANYCORE_DST_BUF_LEN ; i ++ ){
        int volatile returned =pRocketViewTask->result[ tile_id ][ i ];
        if( returned != COMP_RESULT(i,tile_id) ) {
            printf(" (y:%d, x:%d)==> Expect %x, returned %x", y_cord, x_cord, COMP_RESULT(i, tile_id), returned);
            return 1;
        }
    }
    return 0;
}
