#ifndef __BSG_ROCKET_MANYCORE_STREAMBUF__
#define __BSG_ROCKET_MANYCORE_STREAMBUF__


//buffer len in WORDS, must be dividable by 2
//if repeat > 1,  len must be dividable by 4
//#define BUF_LEN      (bsg_tiles_X * bsg_tiles_Y * 2 + 4)
//#define BUF_LEN      2
//#define BUF_LEN      4
//#define BUF_LEN      6
//#define BUF_LEN      (bsg_tiles_X * bsg_tiles_Y)
//#define BUF_LEN      (bsg_tiles_X * bsg_tiles_Y + 2)
//#define BUF_LEN      (bsg_tiles_X * bsg_tiles_Y + 4)
//#define BUF_LEN      (bsg_tiles_X * bsg_tiles_Y + 6)
#define BUF_LEN      (bsg_tiles_X * bsg_tiles_Y*(1024 - 6)  - 6)
#define BUF_REPEAT   1

int buff [BUF_LEN ];

//Internal use, Don't change
#define FIFO_NUM        4

//Internal use, Don't change
#define X_CORD_START    0

//this is the x_cord index of each FIFO
//the OUT_FIFO_DIST should be define in Makefile
//Internal use, Don't change
const int fifo_x_cord_vect[ FIFO_NUM ] = OUT_FIFO_DIST; 
#endif
