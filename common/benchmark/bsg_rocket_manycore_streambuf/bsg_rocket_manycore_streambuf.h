#ifndef __BSG_ROCKET_MANYCORE_STREAMBUF__
#define __BSG_ROCKET_MANYCORE_STREAMBUF__

//buffer len in WORDS,  must be dividable by 2
//if repeat > 1,  len must be dividable by 4
#define BUF_LEN       110  
#define BUF_REPEAT    2

#include "bnn_layer1_params.h"
const int * buff = wt_layer1_uint32t;

//Interal use, Don't change.
#define X_CORD_START    0

//Interal use, Don't change.
#define FIFO_NUM        4

//this is the x_cord index of each FIFO
//the OUT_FIFO_DIST should be define in Makefile
//Internal use, Don't change
const int fifo_x_cord_vect[ FIFO_NUM ] = OUT_FIFO_DIST; 
#endif
