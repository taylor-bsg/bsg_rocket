#=======================================================================
# UCB CS250 Makefile fragment for benchmarks
#-----------------------------------------------------------------------
#
# Each benchmark directory should have its own fragment which
# essentially lists what the source files are and how to link them
# into an riscv and/or host executable. All variables should include
# the benchmark name as a prefix so that they are unique.
#

memcpy_xcel_c_src = \
	memcpy_xcel.c \
	syscalls.c    \

memcpy_xcel_riscv_src = \
	crt.S \

memcpy_xcel_c_objs     = $(patsubst %.c, %.o, $(memcpy_xcel_c_src))
memcpy_xcel_riscv_objs = $(patsubst %.S, %.o, $(memcpy_xcel_riscv_src))

memcpy_xcel_host_bin = memcpy_xcel.host
$(memcpy_xcel_host_bin) : $(memcpy_xcel_c_src)
	$(HOST_COMP) $^ -o $(memcpy_xcel_host_bin)

memcpy_xcel_riscv_bin = memcpy_xcel.riscv
$(memcpy_xcel_riscv_bin) : $(memcpy_xcel_c_objs) $(memcpy_xcel_riscv_objs)
	$(RISCV_LINK) $(memcpy_xcel_c_objs) $(memcpy_xcel_riscv_objs) -o $(memcpy_xcel_riscv_bin) $(RISCV_LINK_OPTS)

junk += $(memcpy_xcel_c_objs) $(memcpy_xcel_riscv_objs) \
        $(memcpy_xcel_host_bin) $(memcpy_xcel_riscv_bin)
