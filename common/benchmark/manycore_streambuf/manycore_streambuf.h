#ifndef __MANYCORE_STREAMBUF__
#define __MANYCORE_STREAMBUF__

uint64_t wt_uint64t[] =
  {
    #include "bnn_params.h"
  };

//buffer len in WORDS,  must be dividable by 2
#define BUF_LEN       (sizeof(wt_uint64t) / sizeof(unsigned int))

//if repeat > 1,  len must be dividable by 4
#ifndef BUF_REPEAT
#define BUF_REPEAT    1
#endif

// Internal use. Those shouldn't be changed
#ifndef X_CORD_START
#define X_CORD_START  0
#endif

#define FIFO_NUM      4

const int * buff = (const int *)wt_uint64t;
//this is the x_cord index of each FIFO
//the OUT_FIFO_DIST should be define in Makefile
const int fifo_x_cord_vect[ FIFO_NUM ] = OUT_FIFO_DIST; 
#endif
