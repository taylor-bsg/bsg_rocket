//====================================================================
// bsg_rocket_manycore_streambuf.c
// 02/08/2016, shawnless.xie@gmail.com
//====================================================================
// The following is a basic RISC-V program to test the functionality of the
// Rocket + manycore, in which manycore just copy data back to Rocket
//#define BASIC_TEST

#include <assert.h>
#include <stdio.h>
#include <stdint.h>

#include "manycore.cfg.h"
#include "bsg_rocket_rocc.h"
#include "bsg_manycore.h"


#include "manycore_streambuf.h"

//symbol defined in manycore.vec.c 
//extern int manycore_mem_vect[];

//symbol defined in manycore/main.S
extern int  sm_data_start   ;
extern int  sm_output_ptr   ; 
extern int  sm_go           ; 
extern int  sm_next_go_ptr  ; 
extern int  sm_data_len     ;
extern int  sm_finish       ; 
extern int  sm_repeat       ; 
//symbol defined in the asm.ld
extern int  _bsg_text_end   ; 



////////////////////////////////////////////////////////////
//
void init_source(int * p_buff, int len);

/////////////////////////////////////////////////////////////
int main() {

  //setup the segment address
  bsg_rocc_seg_addr(  manycore_mem_vect );

  //reset the manycore
  bsg_rocc_reset_manycore();

  int p_bsg_text_end_in_words      = ( int )( & _bsg_text_end  ) / 4;
  //load the manycore image, we only load the text section
  for( int i=0; i< bsg_tiles_Y; i ++ ) 
    for( int j=X_CORD_START ; j < bsg_tiles_X; j ++)
        bsg_rocc_load_manycore_len( i, j, p_bsg_text_end_in_words);

  bsg_rocket_fence( );

  //initial the source data for manycore
  init_source(buff, BUF_LEN);

  //start the manycore
  for( int i=0; i< bsg_tiles_Y; i++ )
    for( int j=X_CORD_START; j< bsg_tiles_X; j++ )
       bsg_rocc_unfreeze( i, j );

  //finish the benchmark
  return 0;
}

/////////////////////////////////////////////////////////////
//init the source data
void init_source( int * p_buff, int len) {
  int group_num             =  FIFO_NUM                         ;
  int x_tiles_num           =  bsg_tiles_X - X_CORD_START       ;
  int x_per_group           =  x_tiles_num / group_num          ;
  int total_tiles_num       =  bsg_tiles_Y * x_tiles_num        ;
  // len must but multiple times of 2
  // total tile number must be multiple times of 2
  int data_per_tiles        =  len / total_tiles_num            ; 
  int remain_data_num       =  len % total_tiles_num            ;

  int remain_data_per_grp       =  remain_data_num / group_num      ;
  int remain_data_per_grp_rm    =  remain_data_num % group_num      ;

  int p_sm_data_start     = ( int )( & sm_data_start  );
  int p_sm_output_ptr     = ( int )( & sm_output_ptr  );
  int p_sm_go             = ( int )( & sm_go          );
  int p_sm_next_go_ptr    = ( int )( & sm_next_go_ptr );
  int p_sm_data_len       = ( int )( & sm_data_len    );
  int p_sm_finish         = ( int )( & sm_finish      );
  int p_sm_repeat         = ( int )( & sm_repeat      );
  
  int *  sm_output_ptr_val      ;   
  int    sm_go_val              ;   
  int *  sm_next_go_ptr_val     ;   
  int    sm_data_len_val        ;   
  int    sm_finish_val          ;   
  int    sm_repeat_val          ;   
  
  #ifdef BASIC_TEST
    for( int i=0; i< BUF_LEN ; i ++ ) 
        buff[ i ] = i+1;
  #endif

  for( int y_cord=0;  y_cord < bsg_tiles_Y ; y_cord ++ ) {
    for( int x_cord = X_CORD_START;  x_cord  < bsg_tiles_X ; x_cord ++ ){
        
        int    shift_x_cord = ( x_cord - X_CORD_START)      ;
        int    group_id     = shift_x_cord  / x_per_group     ;
       
        //output_tpr 
        sm_output_ptr_val = bsg_remote_ptr_io( fifo_x_cord_vect[ group_id ], 0x0) ;
        bsg_rocc_write( y_cord, x_cord,     p_sm_output_ptr,  ( int )sm_output_ptr_val );

        //sm_go, only the first tile in each group will be initialized.
        if( (y_cord == 0x0)  && ( (shift_x_cord % x_per_group) == 0) )    sm_go_val = 0x1;
        else                                                              sm_go_val = 0x0; 
        bsg_rocc_write( y_cord, x_cord,     p_sm_go        ,       sm_go_val        );
       
        //sm_next_go_ptr 
        //Next line in the group ?
        int next_y_cord = ( (shift_x_cord+1) % x_per_group == 0 ) ?  ( y_cord + 1) :  y_cord ; 
        //x_cord_start + group_x_start + (x+1)
        int next_x_cord =   group_id * x_per_group                  \
                        +  ( shift_x_cord + 1 ) % x_per_group       \
                        +  X_CORD_START                             ;

        int first_tile_y_cord = 0x0;
        int first_tile_x_cord = group_id*x_per_group + X_CORD_START ;

        //Last tile in the group
        int is_last_tile = (                 y_cord       == ( bsg_tiles_Y -1 ) \
                        &&   (shift_x_cord % x_per_group) == ( x_per_group -1 ) );
        
        if( is_last_tile )  sm_next_go_ptr_val = bsg_remote_ptr( first_tile_x_cord, first_tile_y_cord, (int)(&sm_go));
        else                sm_next_go_ptr_val = bsg_remote_ptr( next_x_cord, next_y_cord, (int)(&sm_go) );

        bsg_rocc_write( y_cord, x_cord,     p_sm_next_go_ptr , ( int * )sm_next_go_ptr_val );

        //------------------------------
        //sm_data_len
        sm_data_len_val = data_per_tiles * 0x4 ;
        if( is_last_tile ) {
              sm_data_len_val +=  remain_data_per_grp * 0x4 ;
              if( (remain_data_per_grp_rm != 0) && ( group_id  < remain_data_per_grp_rm ) ) 
                sm_data_len_val += 0x4;   
        }
        
        bsg_rocc_write( y_cord, x_cord,     p_sm_data_len    ,      sm_data_len_val    );
        
        //the last tiles should write finish  
        #ifndef BASIC_TEST
                                  sm_finish_val = 0x0           ;
        #else
            if( is_last_tile  )   sm_finish_val = 0xFFFFFFFF    ;
            else                  sm_finish_val = 0x0           ;
        #endif

        bsg_rocc_write( y_cord, x_cord,     p_sm_finish     ,      sm_finish_val      );

        //Write the repeat number to the last tile of each group
        sm_repeat_val = BUF_REPEAT -1 ;
        if( is_last_tile) {
            bsg_rocc_write( y_cord, x_cord,     p_sm_repeat    ,      sm_repeat_val    );
        }

        //initialize the data
        int    inter_group_step    = 0x1 ;
        int    inter_tile_step     = group_num  * data_per_tiles ;   
        int    intra_group_tile_id = y_cord * x_per_group + (shift_x_cord % x_per_group);
        
        int    buff_index_start    = inter_group_step * group_id                \
                                   + inter_tile_step  * intra_group_tile_id     ; 
        int    buff_index   ;
        int    tmp_data_addr;
        for( int i=0 ; i < sm_data_len_val/4 ; i ++) {
            tmp_data_addr   = p_sm_data_start  + i * 4         ;
            buff_index      = buff_index_start + i * group_num ;
            bsg_rocc_write( y_cord, x_cord, tmp_data_addr,  p_buff[ buff_index ] ); 
        }
    }
  }

}
