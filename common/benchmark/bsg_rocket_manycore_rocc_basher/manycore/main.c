
#define MANYCORE_PROG
#include "bsg_manycore.h"

#include "bsg_manycore_buffer.h"

//this macro will defined in Makefile, indicates which rocc can be wrote into
//#define bsg_active_rocc_index

#if (MANYCORE_DST_BUF_LEN != 12)
#error ("MANYCORE_DST_BUF_LEN must be 12")
#endif

#define DECLARE_TMP_VAR( COUNTER )      int tmp##COUNTER
#define INIT_TMP_VAR( COUNTER )         tmp##COUNTER = manycore_data_s.source[ COUNTER ]

int main()
{
  int i;
  int rocket_address=0;

  REPEAT12( DECLARE_TMP_VAR, ;);
  REPEAT12( INIT_TMP_VAR, ;   );

  int * dest_ptr = bsg_remote_ptr_io( bsg_active_rocc_index, ( (unsigned int)(&manycore_data_s.result) + manycore_data_s.base_addr));

  asm volatile ("sw %1,   0x0(%[base_addr]); \
                 sw %2,   0x4(%[base_addr]); \
                 sw %3,   0x8(%[base_addr]); \
                 sw %4,   0xC(%[base_addr]); \
                 sw %5,   0x10(%[base_addr]); \
                 sw %6,   0x14(%[base_addr]); \
                 sw %7,   0x18(%[base_addr]); \
                 sw %8,   0x1C(%[base_addr]); \
                 sw %9,   0x20(%[base_addr]); \
                 sw %10,  0x24(%[base_addr]); \
                 sw %11,  0x28(%[base_addr]); \
                 sw %12,  0x2C(%[base_addr]); \
                " : :                               \
                 [base_addr] "r"( dest_ptr)     \
                , "r"( tmp0 )   \
                , "r"( tmp1 )   \
                , "r"( tmp2 )   \
                , "r"( tmp3 )   \
                , "r"( tmp4 )   \
                , "r"( tmp5 )   \
                , "r"( tmp6 )   \
                , "r"( tmp7 )   \
                , "r"( tmp8 )   \
                , "r"( tmp9 )   \
                , "r"( tmp10)   \
                , "r"( tmp11)   \
                );

  //write the done signal
  rocket_address= ( unsigned int )(&manycore_data_s.done)  + manycore_data_s.base_addr;
  bsg_remote_ptr_io_store(bsg_active_rocc_index, (rocket_address) , 0x1);

  bsg_wait_while(1);
}

////////////////////////////////////////////////////////////////
//Print the current manycore configurations
#pragma message (bsg_VAR_NAME_VALUE( bsg_tiles_X )  )
#pragma message (bsg_VAR_NAME_VALUE( bsg_tiles_Y )  )

#undef DECLARE_TMP_VAR
#undef DECLARE_ADDR_VAR
#undef INIT_TMP_VALUE
#undef INIT_ADDR_VALUE
