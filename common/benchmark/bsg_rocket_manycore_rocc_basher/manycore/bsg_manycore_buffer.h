// This file contains the variable to be used both in manycore and rocket.
#ifndef  _MC_RC_BUFFER_H_
#define  _MC_RC_BUFFER_H_

#ifndef MANYCORE_PROG
#define _MC_RC_PREFIX_ extern volatile

#else
#define _MC_RC_PREFIX_

#endif

///////////////////////////////////////////////////////////////
#ifndef MANYCORE_SRC_BUF_LEN
    #define MANYCORE_SRC_BUF_LEN 12
#endif

#ifndef MANYCORE_DST_BUF_LEN
    #define MANYCORE_DST_BUF_LEN 12
#endif

typedef struct manycore_task_def {
    //the base address that this struct has been mapped.
    //When manycore try to store data back to the rocket, it
    //should added the base_addr to the buffer to get the right
    //address
    unsigned int base_addr;
    int source[ MANYCORE_SRC_BUF_LEN ];
    int done                          ;
    int result[ MANYCORE_DST_BUF_LEN ];
} manycore_task_s;

_MC_RC_PREFIX_  manycore_task_s  manycore_data_s;

#define REPEAT2( EXP, Seperator )   EXP( 0 ) Seperator EXP( 1 )
#define REPEAT3( EXP, Seperator )   REPEAT2(EXP, Seperator)   Seperator EXP( 2 )
#define REPEAT4( EXP, Seperator )   REPEAT3(EXP, Seperator)   Seperator EXP( 3 )
#define REPEAT5( EXP, Seperator )   REPEAT4(EXP, Seperator)   Seperator EXP( 4 )
#define REPEAT6( EXP, Seperator )   REPEAT5(EXP, Seperator)   Seperator EXP( 5 )
#define REPEAT7( EXP, Seperator )   REPEAT6(EXP, Seperator)   Seperator EXP( 6 )
#define REPEAT8( EXP, Seperator )   REPEAT7(EXP, Seperator)   Seperator EXP( 7 )
#define REPEAT9( EXP, Seperator )   REPEAT8(EXP, Seperator)   Seperator EXP( 8 )
#define REPEAT10( EXP, Seperator )  REPEAT9(EXP, Seperator)   Seperator EXP( 9 )
#define REPEAT11( EXP, Seperator )  REPEAT10(EXP, Seperator)  Seperator EXP( 10 )
#define REPEAT12( EXP, Seperator )  REPEAT11(EXP, Seperator)  Seperator EXP( 11 )
#define REPEAT13( EXP, Seperator )  REPEAT12(EXP, Seperator)  Seperator EXP( 12 )
#define REPEAT14( EXP, Seperator )  REPEAT13(EXP, Seperator)  Seperator EXP( 13 )
#define REPEAT15( EXP, Seperator )  REPEAT14(EXP, Seperator)  Seperator EXP( 14 )
#define REPEAT16( EXP, Seperator )  REPEAT15(EXP, Seperator)  Seperator EXP( 15 )

//Define the macros to do loop unrolling

#define bsg_rocc_write12( y_cord, x_cord,local_addr0, value0, \
                                         local_addr1, value1, \
                                         local_addr2, value2, \
                                         local_addr3, value3, \
                                         local_addr4, value4, \
                                         local_addr5, value5, \
                                         local_addr6, value6, \
                                         local_addr7, value7, \
                                         local_addr8, value8, \
                                         local_addr9, value9, \
                                         local_addr10,value10,\
                                         local_addr11,value11 \
     ) \
  asm volatile ("custom0 x0, %0, %1, 0; \
                 custom0 x0, %2, %3, 0; \
                 custom0 x0, %4, %5, 0; \
                 custom0 x0, %6, %7, 0; \
                 custom0 x0, %8, %9, 0; \
                 custom0 x0, %10,%11,0; \
                 custom0 x0, %12,%13, 0; \
                 custom0 x0, %14,%15, 0; \
                 custom0 x0, %16,%17, 0; \
                 custom0 x0, %18,%19, 0; \
                 custom0 x0, %20,%21, 0; \
                 custom0 x0, %22,%23, 0; \
                " : :                               \
                 "r"( local_addr0),  "r"(value0),    \
                 "r"( local_addr1),  "r"(value1),    \
                 "r"( local_addr2),  "r"(value2),    \
                 "r"( local_addr3),  "r"(value3),    \
                 "r"( local_addr4),  "r"(value4),    \
                 "r"( local_addr5),  "r"(value5),    \
                 "r"( local_addr6),  "r"(value6),    \
                 "r"( local_addr7),  "r"(value7),    \
                 "r"( local_addr8),  "r"(value8),    \
                 "r"( local_addr9),  "r"(value9),   \
                 "r"( local_addr10), "r"(value10),    \
                 "r"( local_addr11), "r"(value11)    \
                )

#endif
